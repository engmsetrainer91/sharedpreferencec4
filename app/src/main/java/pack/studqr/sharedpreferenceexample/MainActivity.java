package pack.studqr.sharedpreferenceexample;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
   SharedPreferences sharedPreferences;
    SeekBar sound , brightness;
    Button btnSave,alertDologBtn,timePicker;
    EditText nameEt;
    RadioButton rb1houe,rbDay;
    Switch noiSwitch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sound = findViewById(R.id.soundLevel);
        brightness = findViewById(R.id.brightLevel);
        btnSave = findViewById(R.id.saveSettings);
        nameEt = findViewById(R.id.userName);
        rb1houe = findViewById(R.id.every12Rb);
        rbDay = findViewById(R.id.everyDayRb);
        noiSwitch = findViewById(R.id.switchNoti);
        alertDologBtn = findViewById(R.id.alertDialog);
        timePicker = findViewById(R.id.timePicker);
        sharedPreferences = getSharedPreferences("Settings",MODE_PRIVATE);
        readDataFromSharedPref();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                saveData();
            }
        });
        alertDologBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                dialog.setTitle("Exit Message");
                dialog.setMessage("Are you sure that do you want to exit?");
                dialog.setPositiveButton("Save Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        saveData();
                    }
                });
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                       finish();
                    }
                });
                dialog.show();
            }

        });

        timePicker.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Calendar c = Calendar.getInstance();
                int h = c.get(Calendar.HOUR_OF_DAY);
                int m = c.get(Calendar.MINUTE);
                TimePickerDialog dialog = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        Toast.makeText(MainActivity.this, i+":"+i1, Toast.LENGTH_SHORT).show();
                    }
                },h,m,false);
                dialog.show();
                return true;
            }
        });


    }


    private  void readDataFromSharedPref(){
        noiSwitch.setChecked(sharedPreferences.getBoolean("NOTI_KEY",false));
        nameEt.setText(sharedPreferences.getString("UN_KEY",""));
        sound.setProgress(sharedPreferences.getInt("SOUND_KEY",10));
        brightness.setProgress(sharedPreferences.getInt("BRIG_KEY",20));
        int peroid = sharedPreferences.getInt("NOTI_PERIOD_KEY",1);
        if(peroid==1){
            rb1houe.setChecked(true);
        }else{
            rbDay.setChecked(true);
        }

    }
    private  void saveData(){
        int noti = 1;
        if(rb1houe.isChecked()){
            noti = 1;
        }else{
            noti = 2;
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("NOTI_KEY",noiSwitch.isChecked());
        editor.putString("UN_KEY",nameEt.getText().toString());
        editor.putInt("SOUND_KEY",sound.getProgress());
        editor.putInt("BRIG_KEY",brightness.getProgress());
        editor.putInt("NOTI_PERIOD_KEY",noti);
        editor.apply();
        Toast.makeText(MainActivity.this, "Setting Scussfully", Toast.LENGTH_SHORT).show();
    }
}